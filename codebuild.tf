resource "aws_codebuild_project" "default" {
  name         = "${var.servelerless_service_name}-${var.servelerless_stage}"
  description  = "${var.servelerless_service_name}-${var.servelerless_stage} codebuild"
  service_role = "${aws_iam_role.codebuild.arn}"

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    type            = "LINUX_CONTAINER"
    compute_type    = "BUILD_GENERAL1_SMALL"
    image           = "aws/codebuild/standard:2.0"
    privileged_mode = "true"

    environment_variable {
      "name"  = "AWS_DEFAULT_REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "AWS_ACCOUNT_ID"
      "value" = "${data.aws_caller_identity.current.account_id}"
    }
  }

  source {
    type     = "S3"
    location = "${module.sub-terraform-aws-s3-bucket.bucket_arn}/*"
  }

  build_timeout = "15"
}

resource "aws_iam_role" "codebuild" {
  name               = "${var.servelerless_service_name}-${var.servelerless_stage}-codebuild"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_codebuild.json}"
  path               = "/"
  description        = "Managed by Terraform"
}

data "aws_iam_policy_document" "assume_role_codebuild" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "codebuild" {
  name        = "${var.servelerless_service_name}-${var.servelerless_stage}-codebuild"
  policy      = "${data.aws_iam_policy_document.codebuild.json}"
  path        = "/"
  description = "Manager by Terraform"
}

data "aws_iam_policy_document" "codebuild" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
      "s3:ListBucket",
      "s3:PutObject",
      "s3:PutObjectVersionAcl",
    ]

    resources = [
      "arn:aws:s3:::${var.servelerless_service_name}-${var.servelerless_stage}-codebuild",
      "arn:aws:s3:::${var.servelerless_service_name}-${var.servelerless_stage}-codebuild/*",
    ]
  }
}

resource "aws_iam_role_policy_attachment" "codebuild" {
  role       = "${aws_iam_role.codebuild.name}"
  policy_arn = "${aws_iam_policy.codebuild.arn}"
}
