module "sub-terraform-aws-s3-bucket" {
  #Implantar contador
  #count         = "${var.s3-name == "" ? 1 : 0}"
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-s3-bucket.git?ref=master"

  name          = "${var.s3-name}"
  force_destroy = "${var.s3-force_destroy}"
  tags          = "${var.s3-tags}"
  policy        = "${var.s3-policy}"
  acl           = "${var.s3-acl}"
  versioned     = "${var.s3-versioned}"
}
