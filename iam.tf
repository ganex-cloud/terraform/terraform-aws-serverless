data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

locals {
  deploy_role_arn = "${distinct(compact(concat(list(data.aws_caller_identity.current.account_id), var.deploy_role_arn)))}"
}

### Serverless Cloudformation ###
resource "aws_iam_role" "default" {
  name                  = "${var.servelerless_service_name}-${var.servelerless_stage}-cloudformation"
  assume_role_policy    = "${data.aws_iam_policy_document.assume_role_cloudformation.json}"
  path                  = "/"
  description           = "Managed by Terraform"
  max_session_duration  = "3600"
  force_detach_policies = "true"
}

resource "aws_iam_policy" "default" {
  name        = "${var.servelerless_service_name}-${var.servelerless_stage}-cloudformation"
  policy      = "${data.aws_iam_policy_document.policy_cloudformation.json}"
  path        = "/"
  description = "Managed by Terraform"
}

resource "aws_iam_role_policy_attachment" "default" {
  role       = "${aws_iam_role.default.name}"
  policy_arn = "${aws_iam_policy.default.arn}"
}

data "aws_iam_policy_document" "policy_cloudformation" {
  statement {
    actions = [
      "iam:GetRole",
      "iam:PassRole",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:DetachRolePolicy",
      "iam:PutRolePolicy",
      "iam:AttachRolePolicy",
      "iam:DeleteRolePolicy",
      "iam:getRolePolicy",
      "iam:ListRoleTags",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.servelerless_service_name}-${var.servelerless_stage}*-lambdaRole",
    ]
  }

  statement {
    actions = [
      "cloudformation:Describe*",
      "cloudformation:List*",
      "cloudformation:Get*",
      "cloudformation:PreviewStackUpdate",
      "cloudformation:CreateStack",
      "cloudformation:UpdateStack",
    ]

    resources = [
      "arn:aws:cloudformation:us-east-1:${data.aws_caller_identity.current.account_id}:stack/${var.servelerless_service_name}-${var.servelerless_stage}*",
    ]
  }

  statement {
    actions = [
      "cloudformation:ValidateTemplate",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "s3:ListAllMyBuckets",
      "s3:CreateBucket",
      "s3:DeleteBucket",
      "s3:DeleteBucketPolicy",
      "s3:GetEncryptionConfiguration",
      "s3:PutEncryptionConfiguration",
      "s3:GetObject",
      "s3:PutBucketPolicy",
    ]

    resources = [
      "arn:aws:s3:::${var.servelerless_service_name}-*",
      "arn:aws:s3:::${var.servelerless_service_name}-*/*",
    ]
  }

  statement {
    actions = [
      "apigateway:GET",
      "apigateway:HEAD",
      "apigateway:OPTIONS",
      "apigateway:PATCH",
      "apigateway:POST",
      "apigateway:PUT",
      "apigateway:DELETE",
    ]

    resources = [
      "arn:aws:apigateway:*::/restapis",
      "arn:aws:apigateway:*::/restapis/*",
    ]
  }

  statement {
    actions = [
      "logs:DescribeLogGroups",
    ]

    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group::log-stream:*",
    ]
  }

  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DeleteLogGroup",
      "logs:DeleteLogStream",
      "logs:DescribeLogStreams",
      "logs:FilterLogEvents",
    ]

    resources = [
      "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.servelerless_service_name}-${var.servelerless_stage}*:log-stream:*",
    ]
  }

  statement {
    actions = [
      "events:DescribeRule",
      "events:PutRule",
      "events:PutTargets",
      "events:RemoveTargets",
      "events:DeleteRule",
    ]

    resources = [
      "arn:aws:events:us-east-1:${data.aws_caller_identity.current.account_id}:rule/${var.servelerless_service_name}-${var.servelerless_stage}*",
    ]
  }

  statement {
    actions = [
      "cloudformation:ListStacks",
      "cloudformation:PreviewStackUpdate",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "lambda:GetFunction",
      "lambda:CreateFunction",
      "lambda:DeleteFunction",
      "lambda:UpdateFunctionConfiguration",
      "lambda:UpdateFunctionCode",
      "lambda:ListVersionsByFunction",
      "lambda:PublishVersion",
      "lambda:CreateAlias",
      "lambda:DeleteAlias",
      "lambda:UpdateAlias",
      "lambda:GetFunctionConfiguration",
      "lambda:AddPermission",
      "lambda:RemovePermission",
      "lambda:InvokeFunction",
      "lambda:ListTags",
      "lambda:TagResource",
      "lambda:UntagResource",
    ]

    resources = [
      "arn:aws:lambda:*:${data.aws_caller_identity.current.account_id}:function:${var.servelerless_service_name}-${var.servelerless_stage}*",
    ]
  }

  statement {
    actions = [
      "lambda:GetLayerVersion",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeVpcs",
      "ec2:DescribeNetworkInterfaces",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "lambda:DeleteEventSourceMapping",
      "lambda:CreateEventSourceMapping",
      "lambda:GetEventSourceMapping",
      "lambda:UpdateEventSourceMapping",
    ]

    resources = [
      "*",
    ]
  }
}

data "aws_iam_policy_document" "assume_role_cloudformation" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["cloudformation.amazonaws.com"]
    }
  }
}

### Serverless Deploy Policy ###
resource "aws_iam_role" "deploy" {
  name                  = "${var.servelerless_service_name}-${var.servelerless_stage}-deploy"
  assume_role_policy    = "${data.aws_iam_policy_document.assume_role_deploy.json}"
  path                  = "/"
  description           = "Managed by Terraform"
  max_session_duration  = "3600"
  force_detach_policies = "true"
}

resource "aws_iam_policy" "deploy" {
  name   = "${var.servelerless_service_name}-${var.servelerless_stage}-deploy"
  path   = "/"
  policy = "${data.aws_iam_policy_document.policy_deploy.json}"
}

resource "aws_iam_group" "deploy" {
  name = "${var.servelerless_service_name}-${var.servelerless_stage}-deploy"
}

resource "aws_iam_group_policy_attachment" "deploy" {
  group      = "${aws_iam_group.deploy.name}"
  policy_arn = "${aws_iam_policy.deploy.arn}"
  depends_on = ["aws_iam_policy.deploy"]
}

resource "aws_iam_role_policy_attachment" "deploy" {
  role       = "${aws_iam_role.deploy.name}"
  policy_arn = "${aws_iam_policy.deploy.arn}"
}

data "aws_iam_policy_document" "assume_role_deploy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }

  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["${local.deploy_role_arn}"]
    }
  }
}

data "aws_iam_policy_document" "policy_deploy" {
  statement {
    actions = [
      "iam:PassRole",
    ]

    resources = [
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.servelerless_service_name}-${var.servelerless_stage}-cloudformation",
      "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.servelerless_service_name}-${var.servelerless_stage}-*-lambdaRole",
    ]
  }

  statement {
    actions = [
      "s3:ListBucketVersions",
      "s3:PutObject",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:DeleteObject",
      "s3:CreateBucket",
      "s3:DeleteBucket",
      "s3:GetEncryptionConfiguration",
      "s3:PutEncryptionConfiguration",
      "s3:GetBucketLocation",
    ]

    resources = [
      "arn:aws:s3:::${var.servelerless_service_name}-*",
      "arn:aws:s3:::${var.servelerless_service_name}-*/*",
    ]
  }

  statement {
    actions = [
      "cloudformation:CreateStack",
      "cloudformation:UpdateStack",
      "cloudformation:DeleteStack",
    ]

    resources = [
      "arn:aws:cloudformation:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:stack/${var.servelerless_service_name}-${var.servelerless_stage}*",
    ]
  }

  statement {
    actions = [
      "cloudformation:Describe*",
      "cloudformation:List*",
      "cloudformation:Get*",
      "cloudformation:PreviewStackUpdate",
      "cloudformation:ValidateTemplate",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "apigateway:GET",
      "apigateway:HEAD",
      "apigateway:OPTIONS",
      "apigateway:PATCH",
      "apigateway:POST",
      "apigateway:PUT",
      "apigateway:DELETE",
    ]

    resources = [
      "arn:aws:apigateway:*::/restapis",
      "arn:aws:apigateway:*::/restapis/*",
    ]
  }
}
