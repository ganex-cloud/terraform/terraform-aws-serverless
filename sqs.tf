module "sub-terraform-aws-sqs" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-sqs.git?ref=master"

  #Implantar contador
  #count                       = "${var.sqs-name == "" ? 1 : 0}"

  name                              = "${var.sqs-name}"
  visibility_timeout_seconds        = "${var.sqs-visibility_timeout_seconds}"
  message_retention_seconds         = "${var.sqs-message_retention_seconds}"
  max_message_size                  = "${var.sqs-max_message_size}"
  delay_seconds                     = "${var.sqs-delay_seconds}"
  receive_wait_time_seconds         = "${var.sqs-receive_wait_time_seconds}"
  policy                            = "${var.sqs-policy}"
  redrive_policy                    = "${var.sqs-redrive_policy}"
  fifo_queue                        = "${var.sqs-fifo_queue}"
  content_based_deduplication       = "${var.sqs-content_based_deduplication}"
  tags                              = "${var.sqs-tags}"
  kms_master_key_id                 = "${var.sqs-kms_master_key_id}"
  kms_data_key_reuse_period_seconds = "${var.sqs-kms_data_key_reuse_period_seconds}"
}
