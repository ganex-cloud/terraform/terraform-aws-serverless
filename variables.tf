variable "servelerless_service_name" {
  type        = "string"
  description = "Serverless name of service (function)"
}

variable "servelerless_stage" {
  type        = "string"
  description = "Serverless stage (environment)"
}

variable "deploy_role_arn" {
  description = "List of roles ARN that are trusted to use deploy role."
  type        = "list"
  default     = []
}

# Variáveis para o S3

variable "s3-name" {
  description = "Name of bucket"
}

variable "s3-force_destroy" {
  description = "Delete all objects in bucket on destroy"
  default     = false
}

variable "s3-versioned" {
  description = "Version the bucket"
  default     = false
}

variable "s3-policy" {
  default     = ""
  description = "A valid bucket policy JSON document"
}

variable "s3-tags" {
  type        = "map"
  description = "Additional Tags"
  default     = {}
}

variable "s3-acl" {
  description = "ACL"
  default     = "private"
}

# Variáveis para o SQS

variable "sqs-create" {
  description = "Whether to create SQS queue"
  default     = true
}

variable "sqs-sqs_queue_with_kms" {
  description = "Whether to create SQS queue with KMS encryption"
  default     = false
}

variable "sqs-name" {
  description = "This is the human-readable name of the queue. If omitted, Terraform will assign a random name."
  default     = ""
}

variable "sqs-visibility_timeout_seconds" {
  description = "The visibility timeout for the queue. An integer from 0 to 43200 (12 hours)"
  default     = 30
}

variable "sqs-message_retention_seconds" {
  description = "The number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days)"
  default     = 345600
}

variable "sqs-max_message_size" {
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it. An integer from 1024 bytes (1 KiB) up to 262144 bytes (256 KiB)"
  default     = 262144
}

variable "sqs-delay_seconds" {
  description = "The time in seconds that the delivery of all messages in the queue will be delayed. An integer from 0 to 900 (15 minutes)"
  default     = 0
}

variable "sqs-receive_wait_time_seconds" {
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning. An integer from 0 to 20 (seconds)"
  default     = 0
}

variable "sqs-policy" {
  description = "The JSON policy for the SQS queue"
  default     = ""
}

variable "sqs-redrive_policy" {
  description = "The JSON policy to set up the Dead Letter Queue, see AWS docs. Note: when specifying maxReceiveCount, you must specify it as an integer (5), and not a string (\"5\")"
  default     = ""
}

variable "sqs-fifo_queue" {
  description = "Boolean designating a FIFO queue"
  default     = false
}

variable "sqs-content_based_deduplication" {
  description = "Enables content-based deduplication for FIFO queues"
  default     = false
}

variable "sqs-kms_master_key_id" {
  description = "The ID of an AWS-managed customer master key (CMK) for Amazon SQS or a custom CMK"
  default     = ""
}

variable "sqs-kms_data_key_reuse_period_seconds" {
  description = "The length of time, in seconds, for which Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again. An integer representing seconds, between 60 seconds (1 minute) and 86,400 seconds (24 hours)"
  default     = 300
}

variable "sqs-tags" {
  description = "A mapping of tags to assign to all resources"
  default     = {}
}

# Variáveis para o CodeBuild


#variable "codebuild-name" {
#  type        = "string"
#  description = "The projects name."
#}
#
#variable "codebuild-s3_bucket_arn" {
#  type        = "string"
#  description = "The S3 Bucket ARN"
#}
#
#variable "codebuild-s3_bucket_path" {
#  default     = "/source.zip"
#  type        = "string"
#  description = "The S3 object Key or folder"
#}
#
#variable "codebuild-environment_type" {
#  default     = "LINUX_CONTAINER"
#  type        = "string"
#  description = "The type of build environment to use for related builds."
#}
#
#variable "codebuild-compute_type" {
#  default     = "BUILD_GENERAL1_SMALL"
#  type        = "string"
#  description = "Information about the compute resources the build project will use."
#}
#
#variable "codebuild-image" {
#  default     = "aws/codebuild/docker:18.09.0"
#  type        = "string"
#  description = "The image identifier of the Docker image to use for this build project."
#}
#
#variable "codebuild-privileged_mode" {
#  default     = true
#  type        = "string"
#  description = "If set to true, enables running the Docker daemon inside a Docker container."
#}
#
#variable "codebuild-encryption_key" {
#  default     = ""
#  type        = "string"
#  description = "The KMS CMK to be used for encrypting the build project's build output artifacts."
#}
#
#variable "codebuild-build_timeout" {
#  default     = 15
#  type        = "string"
#  description = "How long in minutes to wait until timing out any related build that does not get marked as completed."
#}
#
#variable "codebuild-iam_path" {
#  default     = "/"
#  type        = "string"
#  description = "Path in which to create the IAM Role and the IAM Policy."
#}
#
#variable "codebuild-iam_name" {
#  type        = "string"
#  description = "Name of IAM Role and the IAM Policy."
#}
#
#variable "codebuild-description" {
#  default     = "Managed by Terraform"
#  type        = "string"
#  description = "The description of the all resources."
#}
#
#variable "codebuild-tags" {
#  default     = {}
#  type        = "map"
#  description = "A mapping of tags to assign to all resources."
#}
#
#variable "codebuild-role_arn" {
#  description = "Role ARN that are trusted to use codebuild."
#  type        = "list"
#  default     = []
#}
#
#variable "codebuild-environment_variables" {
#  type = "list"
#
#  default = [{
#    "name"  = "NO_ADDITIONAL_BUILD_VARS"
#    "value" = "TRUE"
#  }]
#
#  description = "A list of maps, that contain both the key 'name' and the key 'value' to be used as additional environment variables for the build."
#}
#
#variable "codebuild-aws_region" {
#  type        = "string"
#  default     = ""
#  description = "(Optional) AWS Region, e.g. us-east-1. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
#}
#
#variable "codebuild-aws_account_id" {
#  type        = "string"
#  default     = ""
#  description = "(Optional) AWS Account ID. Used as CodeBuild ENV variable when building Docker images. For more info: http://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html"
#}
#
#variable "codebuild-custom_policy" {
#  type        = "string"
#  default     = ""
#  description = "The custom extra policy document. This is a JSON formatted string."
#}
#
#variable "codebuild-vpc_id" {
#  description = "ID of VPC to run within"
#  default     = ""
#}
#
#variable "codebuild-subnets" {
#  description = "IDs of subnets to run within"
#  default     = []
#}
#
#variable "codebuild-security_groups" {
#  description = "IDs of security groups to attach"
#  default     = []
#}

